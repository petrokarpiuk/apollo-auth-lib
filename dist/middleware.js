'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.verify = verify;
var jwt = require('jsonwebtoken');

function verify(req, res, next, secret) {
  if (req.decoded) return next();

  var token = req.body.token || req.query.token || req.headers['Authorization'];

  if (!token) return res.status(403).send({ success: false, massage: 'no token provided' });

  jwt.verify(token, secret, function (err, decoded) {
    if (err) return res.json({ success: false, message: 'Failed to authenticate token' });

    req.decoded = decoded;

    next();
  });
}