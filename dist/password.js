'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.createHash = createHash;
exports.validateHash = validateHash;

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SaltLength = 9;

function createHash(password) {
  var salt = generateSalt(SaltLength);
  var hash = md5(password + salt);

  return salt + hash;
}

function validateHash(hash, password) {
  var salt = hash.substr(0, SaltLength);
  var validHash = salt + md5(password + salt);

  return hash === validHash;
}

function generateSalt(len) {
  var set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ';
  var setLen = set.length;
  var salt = '';

  for (var i = 0; i < len; i++) {
    var p = Math.floor(Math.random() * setLen);

    salt += set[p];
  }

  return salt;
}

function md5(string) {
  return _crypto2.default.createHash('md5').update(string).digest('hex');
}