import crypto from 'crypto';

const SaltLength = 9

export function createHash(password) {
  const salt = generateSalt(SaltLength)
  const hash = md5(password + salt)
  
  return salt + hash
}

export function validateHash(hash, password) {
  const salt = hash.substr(0, SaltLength)
  const validHash = salt + md5(password + salt)

  return hash === validHash;
}

function generateSalt(len) {
  let set = '0123456789abcdefghijklmnopqurstuvwxyzABCDEFGHIJKLMNOPQURSTUVWXYZ'
  let setLen = set.length
  let salt = ''

  for (let i = 0; i < len; i++) {
    let p = Math.floor(Math.random() * setLen)

    salt += set[p]
  }

  return salt
}

function md5(string) {
  return crypto.createHash('md5').update(string).digest('hex')
}