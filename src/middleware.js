const jwt = require('jsonwebtoken')

export function verify(req, res, next, secret) {
  if (req.decoded) return next()

  const token = req.body.token || req.query.token || req.headers['Authorization']

  if (!token) return res.status(403).send({ success: false, massage: 'no token provided' })

  jwt.verify(token, secret, (err, decoded) => {
    if (err) return res.json({ success: false, message: 'Failed to authenticate token' })

    req.decoded = decoded

    next()
  })
}